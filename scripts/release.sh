#!/bin/bash

git pull --rebase
last_patch=$(git tag | sort -V | tail -n1 | cut -d "." -f3)
git tag "1.0.$((last_patch + 1))"
git push gitlab main -o ci.skip
git push gitlab --tags
git push gitea main
git push gitea --tags
