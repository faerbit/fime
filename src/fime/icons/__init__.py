# Resource object code (Python 3)
# Created by: object code
# Created by: The Resource Compiler for Qt version 5.15.2
# WARNING! All changes made in this file will be lost!

try:
    from PySide6 import QtCore
except ImportError:
    from PySide2 import QtCore

qt_resource_data = b"\
\x00\x00\x02x\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 viewBox=\x22\
0 0 24 24\x22 width\
=\x2224\x22 height=\x2224\
\x22>\x0a  <defs id=\x22d\
efs3051\x22>\x0a    <s\
tyle type=\x22text/\
css\x22 id=\x22current\
-color-scheme\x22>\x0a\
      .ColorSche\
me-Text {\x0a      \
  color:#232629;\
\x0a      }\x0a      <\
/style>\x0a  </defs\
>\x0a  <g transform\
=\x22translate(1,1)\
\x22>\x0a    <path sty\
le=\x22fill:current\
Color;fill-opaci\
ty:1;stroke:none\
\x22 d=\x22M 4 3 L 4 5\
 L 5 5 L 5 4 L 1\
7 4 L 17 5 L 18 \
5 L 18 3 L 17 3 \
L 5 3 L 4 3 z M \
11 5 L 5 11.1835\
94 L 5.7929688 1\
2 L 11 6.6328125\
 L 16.207031 12 \
L 17 11.183594 L\
 11 5 z M 8 13 L\
 8 19 L 9 19 L 1\
3 19 L 14 19 L 1\
4 13 L 13 13 L 1\
3 18 L 9 18 L 9 \
13 L 8 13 z \x22 cl\
ass=\x22ColorScheme\
-Text\x22/>\x0a  </g>\x0a\
</svg>\x0a\
\x00\x00\x02\xdd\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 viewBox=\x22\
0 0 32 32\x22>\x0a  <d\
efs id=\x22defs3051\
\x22>\x0a    <style ty\
pe=\x22text/css\x22 id\
=\x22current-color-\
scheme\x22>\x0a      .\
ColorScheme-Text\
 {\x0a        color\
:#232629;\x0a      \
}\x0a      </style>\
\x0a  </defs>\x0a <pat\
h style=\x22fill:cu\
rrentColor;fill-\
opacity:1;stroke\
:none\x22 \x0a       d\
=\x22M16 4A12 12 0 \
0 0 7.886719 7.1\
79688L7.885 7.17\
8A12 12 0 0 0 7.\
847656 7.214844 \
12 12 0 0 0 4 16\
 12 12 0 0 0 16 \
28 12 12 0 0 0 2\
4.11328 24.82031\
2L24.12 24.822A1\
2 12 0 0 0 24.15\
234 24.785156 12\
 12 0 0 0 28 16 \
12 12 0 0 0 16 4\
M16 5A11 11 0 0 \
1 27 16 11 11 0 \
0 1 24.11523 23.\
408203L8.592 7.8\
85A11 11 0 0 1 1\
6 5M7.885 8.592L\
23.408 24.12A11 \
11 0 0 1 16 27 1\
1 11 0 0 1 5 16 \
11 11 0 0 1 7.88\
4766 8.591797\x22\x0a \
    class=\x22Color\
Scheme-Text\x22\x0a   \
  />\x0a</svg>\x0a\
\x00\x00\x02`\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 viewBox=\x22\
0 0 32 32\x22>\x0a  <d\
efs\x0a     id=\x22def\
s3051\x22>\x0a    <sty\
le\x0a       type=\x22\
text/css\x22\x0a      \
 id=\x22current-col\
or-scheme\x22>\x0a    \
  .ColorScheme-T\
ext {\x0a        co\
lor:#232629;\x0a   \
   }\x0a      </sty\
le>\x0a  </defs>\x0a  \
<path\x0a     style\
=\x22fill:currentCo\
lor;fill-opacity\
:1;stroke:none\x22 \
\x0a     d=\x22M 16 4 \
A 12 12 0 0 0 4 \
16 A 12 12 0 0 0\
 16 28 A 12 12 0\
 0 0 28 16 A 12 \
12 0 0 0 16 4 z \
M 16 5 A 11 11 0\
 0 1 27 16 A 11 \
11 0 0 1 16 27 A\
 11 11 0 0 1 5 1\
6 A 11 11 0 0 1 \
16 5 z M 15 7 L \
15 17 L 24 17 L \
24 16 L 16 16 L \
16 7 L 15 7 z \x22\x0a\
     id=\x22path78\x22\
 \x0a     class=\x22Co\
lorScheme-Text\x22\x0a\
     />\x0a</svg>\x0a\
\x00\x00\x01L\
<\
svg viewBox=\x220 0\
 32 32\x22 xmlns=\x22h\
ttp://www.w3.org\
/2000/svg\x22>\x0a    \
<style\x0a        t\
ype=\x22text/css\x22\x0a \
       id=\x22curre\
nt-color-scheme\x22\
>\x0a        .Color\
Scheme-Text {\x0a  \
          color:\
#232629;\x0a       \
 }\x0a    </style>\x0a\
    <path d=\x22M23\
.707 16l-14-14L9\
 2.707 22.293 16\
 9 29.293l.707.7\
07z\x22 class=\x22Colo\
rScheme-Text\x22 fi\
ll=\x22currentColor\
\x22/>\x0a</svg>\x0a\
\x00\x00\x02g\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 version=\x22\
1.1\x22 viewBox=\x220 \
0 24 24\x22 width=\x22\
24\x22 height=\x2224\x22>\
\x0a  <style type=\x22\
text/css\x22 id=\x22cu\
rrent-color-sche\
me\x22>\x0a        .Co\
lorScheme-Negati\
veText {\x0a       \
     color:#da44\
53;\x0a        }\x0a  \
  </style>\x0a  <g \
transform=\x22trans\
late(1,1)\x22>\x0a    \
<path class=\x22Col\
orScheme-Negativ\
eText\x22 d=\x22M 3.69\
92188 3 L 3 3.69\
92188 L 10.30078\
1 11 L 3 18.3007\
81 C 3 18.300781\
 3.7112147 18.99\
3333 3.6992188 1\
9 L 11 11.699219\
 L 18.300781 19 \
C 18.288781 18.9\
933 19 18.300781\
 19 18.300781 L \
11.699219 11.001\
953 L 19 3.69921\
88 L 18.300781 3\
 L 11 10.300781 \
L 3.6992188 3 z \
\x22 fill=\x22currentC\
olor\x22/>\x0a  </g>\x0a<\
/svg>\x0a\
\x00\x00\x01K\
<\
svg viewBox=\x220 0\
 32 32\x22 xmlns=\x22h\
ttp://www.w3.org\
/2000/svg\x22>\x0a    \
<style\x0a        t\
ype=\x22text/css\x22\x0a \
       id=\x22curre\
nt-color-scheme\x22\
>\x0a        .Color\
Scheme-Text {\x0a  \
          color:\
#232629;\x0a       \
 }\x0a    </style>\x0a\
    <path d=\x22M8.\
293 16l14 14 .70\
7-.707L9.707 16 \
23 2.707 22.293 \
2z\x22 class=\x22Color\
Scheme-Text\x22 fil\
l=\x22currentColor\x22\
/>\x0a</svg>\x0a\
\x00\x00\x01\xe6\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 viewBox=\x22\
0 0 24 24\x22 width\
=\x2224\x22 height=\x2224\
\x22>\x0a  <defs id=\x22d\
efs3051\x22>\x0a    <s\
tyle type=\x22text/\
css\x22 id=\x22current\
-color-scheme\x22>\x0a\
      .ColorSche\
me-Text {\x0a      \
  color:#232629;\
\x0a      }\x0a      <\
/style>\x0a  </defs\
>\x0a  <g transform\
=\x22translate(1,1)\
\x22>\x0a    <path sty\
le=\x22fill:current\
Color;fill-opaci\
ty:1;stroke:none\
\x22 d=\x22M 10 4 L 10\
 11 L 3 11 L 3 1\
2 L 10 12 L 10 1\
9 L 11 19 L 11 1\
2 L 18 12 L 18 1\
1 L 11 11 L 11 4\
 L 10 4 z \x22 clas\
s=\x22ColorScheme-T\
ext\x22/>\x0a  </g>\x0a</\
svg>\x0a\
\x00\x00\x02`\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 viewBox=\x22\
0 0 32 32\x22>\x0a  <d\
efs\x0a     id=\x22def\
s3051\x22>\x0a    <sty\
le\x0a       type=\x22\
text/css\x22\x0a      \
 id=\x22current-col\
or-scheme\x22>\x0a    \
  .ColorScheme-T\
ext {\x0a        co\
lor:#dcd9d6;\x0a   \
   }\x0a      </sty\
le>\x0a  </defs>\x0a  \
<path\x0a     style\
=\x22fill:currentCo\
lor;fill-opacity\
:1;stroke:none\x22 \
\x0a     d=\x22M 16 4 \
A 12 12 0 0 0 4 \
16 A 12 12 0 0 0\
 16 28 A 12 12 0\
 0 0 28 16 A 12 \
12 0 0 0 16 4 z \
M 16 5 A 11 11 0\
 0 1 27 16 A 11 \
11 0 0 1 16 27 A\
 11 11 0 0 1 5 1\
6 A 11 11 0 0 1 \
16 5 z M 15 7 L \
15 17 L 24 17 L \
24 16 L 16 16 L \
16 7 L 15 7 z \x22\x0a\
     id=\x22path78\x22\
 \x0a     class=\x22Co\
lorScheme-Text\x22\x0a\
     />\x0a</svg>\x0a\
\x00\x00\x01L\
<\
svg viewBox=\x220 0\
 32 32\x22 xmlns=\x22h\
ttp://www.w3.org\
/2000/svg\x22>\x0a    \
<style\x0a        t\
ype=\x22text/css\x22\x0a \
       id=\x22curre\
nt-color-scheme\x22\
>\x0a        .Color\
Scheme-Text {\x0a  \
          color:\
#232629;\x0a       \
 }\x0a    </style>\x0a\
    <path d=\x22M23\
.707 16l-14-14L9\
 2.707 22.293 16\
 9 29.293l.707.7\
07z\x22 class=\x22Colo\
rScheme-Text\x22 fi\
ll=\x22currentColor\
\x22/>\x0a</svg>\x0a\
\x00\x00\x01c\
<\
svg viewBox=\x220 0\
 32 32\x22 xmlns=\x22h\
ttp://www.w3.org\
/2000/svg\x22><styl\
e type=\x22text/css\
\x22 id=\x22current-co\
lor-scheme\x22>.Col\
orScheme-Text{co\
lor:#232629;}</s\
tyle><path d=\x22m3\
0 6.5961948-19.3\
03301 19.3032992\
-8.696699-8.6966\
97.7071076-.7071\
06 7.9895914 7.9\
89592 18.596195-\
18.5961947z\x22 sty\
le=\x22fill:current\
Color;fill-opaci\
ty:1;stroke:none\
\x22 class=\x22ColorSc\
heme-Text\x22/></sv\
g>\
\x00\x00\x01\xa2\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 viewBox=\x22\
0 0 24 24\x22 width\
=\x2224\x22 height=\x2224\
\x22>\x0a  <defs id=\x22d\
efs3051\x22>\x0a    <s\
tyle type=\x22text/\
css\x22 id=\x22current\
-color-scheme\x22>\x0a\
      .ColorSche\
me-Text {\x0a      \
  color:#232629;\
\x0a      }\x0a      <\
/style>\x0a  </defs\
>\x0a  <g transform\
=\x22translate(1,1)\
\x22>\x0a    <path sty\
le=\x22fill:current\
Color\x22 class=\x22Co\
lorScheme-Text\x22 \
d=\x22M 3 7 L 3 9 L\
 13 9 L 13 7 L 3\
 7 z\x22 transform=\
\x22translate(3 3)\x22\
/>\x0a  </g>\x0a</svg>\
\x0a\
"

qt_resource_name = b"\
\x00\x05\
\x00o\xa6S\
\x00i\
\x00c\x00o\x00n\x00s\
\x00\x10\
\x03\x84Y\x07\
\x00c\
\x00l\x00o\x00u\x00d\x00-\x00u\x00p\x00l\x00o\x00a\x00d\x00.\x00s\x00v\x00g\
\x00\x11\
\x03S\x8a\x87\
\x00d\
\x00i\x00a\x00l\x00o\x00g\x00-\x00c\x00a\x00n\x00c\x00e\x00l\x00.\x00s\x00v\x00g\
\
\x00\x13\
\x03gvg\
\x00a\
\x00p\x00p\x00o\x00i\x00n\x00t\x00m\x00e\x00n\x00t\x00-\x00n\x00e\x00w\x00.\x00s\
\x00v\x00g\
\x00\x0f\
\x0f\x22iG\
\x00a\
\x00r\x00r\x00o\x00w\x00-\x00r\x00i\x00g\x00h\x00t\x00.\x00s\x00v\x00g\
\x00\x16\
\x0e\x92\x91G\
\x00e\
\x00d\x00i\x00t\x00-\x00d\x00e\x00l\x00e\x00t\x00e\x00-\x00r\x00e\x00m\x00o\x00v\
\x00e\x00.\x00s\x00v\x00g\
\x00\x0e\
\x08\xfa8\xa7\
\x00a\
\x00r\x00r\x00o\x00w\x00-\x00l\x00e\x00f\x00t\x00.\x00s\x00v\x00g\
\x00\x0c\
\x09\xc6\x14\xa7\
\x00l\
\x00i\x00s\x00t\x00-\x00a\x00d\x00d\x00.\x00s\x00v\x00g\
\x00\x19\
\x0b9\x06g\
\x00a\
\x00p\x00p\x00o\x00i\x00n\x00t\x00m\x00e\x00n\x00t\x00-\x00n\x00e\x00w\x00-\x00l\
\x00i\x00g\x00h\x00t\x00.\x00s\x00v\x00g\
\x00\x0b\
\x0c+\x12G\
\x00g\
\x00o\x00-\x00n\x00e\x00x\x00t\x00.\x00s\x00v\x00g\
\x00\x0d\
\x09\xae0'\
\x00d\
\x00i\x00a\x00l\x00o\x00g\x00-\x00o\x00k\x00.\x00s\x00v\x00g\
\x00\x0f\
\x020\x86g\
\x00l\
\x00i\x00s\x00t\x00-\x00r\x00e\x00m\x00o\x00v\x00e\x00.\x00s\x00v\x00g\
"

qt_resource_struct = b"\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x01\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x0b\x00\x00\x00\x02\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x01\x94\x00\x00\x00\x00\x00\x01\x00\x00\x13\xd0\
\x00\x00\x01}.b\xfd-\
\x00\x00\x006\x00\x00\x00\x00\x00\x01\x00\x00\x02|\
\x00\x00\x01}.a\xe4+\
\x00\x00\x00^\x00\x00\x00\x00\x00\x01\x00\x00\x05]\
\x00\x00\x01}.\x1a\xb9\xf9\
\x00\x00\x00\x10\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\
\x00\x00\x01}fHlR\
\x00\x00\x00\xe0\x00\x00\x00\x00\x00\x01\x00\x00\x0b|\
\x00\x00\x01}.b $\
\x00\x00\x01t\x00\x00\x00\x00\x00\x01\x00\x00\x12i\
\x00\x00\x01}.a\xce_\
\x00\x00\x01\x02\x00\x00\x00\x00\x00\x01\x00\x00\x0c\xcb\
\x00\x00\x01}.b\xe1.\
\x00\x00\x01 \x00\x00\x00\x00\x00\x01\x00\x00\x0e\xb5\
\x00\x00\x01}/\x83\xea~\
\x00\x00\x01X\x00\x00\x00\x00\x00\x01\x00\x00\x11\x19\
\x00\x00\x01}.b\x05\xfc\
\x00\x00\x00\xae\x00\x00\x00\x00\x00\x01\x00\x00\x09\x11\
\x00\x00\x01}fHlR\
\x00\x00\x00\x8a\x00\x00\x00\x00\x00\x01\x00\x00\x07\xc1\
\x00\x00\x01}.b1)\
"

def qInitResources():
    QtCore.qRegisterResourceData(0x03, qt_resource_struct, qt_resource_name, qt_resource_data)

def qCleanupResources():
    QtCore.qUnregisterResourceData(0x03, qt_resource_struct, qt_resource_name, qt_resource_data)

qInitResources()
