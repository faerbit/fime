#!/bin/sh
rcc resources.qrc -g python -o __init__.py
sed -i 's/^.*import QtCore.*$/try:\n    from PySide6 import QtCore\nexcept ImportError:\n    from PySide2 import QtCore/g' __init__.py
