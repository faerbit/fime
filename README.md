# fime

Simple time tracking app written with Python and Qt

Some Jira integration is built in.

## Installation, Qt versions and You
Fime supports using both Qt 5 and Qt 6 via their respective PySide packages.

On Linux I recommend installing the PySide package provided by the distribution.
Install `PySide2` for Qt 5 or `PySide6` for Qt 6.

Run `pip install --user fime` in that case.

On other OSes or if you decide not to follow my recommendation run either
 * `pip install --user fime[qt5]` or
 * `pip install --user fime[qt6]`

Note that it both versions of PySide are installed Qt 6 will be preferred.

If you don't want to deal with this, you can download pre-packaged executables from the [Releases](https://gitlab.com/faerbit/fime/-/releases) section.

## Configuration

You probably want to configure the App in the settings dialog before you start using  it.

The Jira URL will just be the base URL to you're Jira Instance (e.g. `https://jira.company.com`)

The Jira Token is a Personal Access Token. See [here](https://confluence.atlassian.com/enterprise/using-personal-access-tokens-1026032365.html) on how to get one.

There is a new authentication method, which uses the cookies from your browser (most popular browsers should be
supported). To use it, you configure it and log into your Jira instance in your browser. If it doesn't work, try closing
your browser, to force it to write the cookies to the disk

## License

Licensed under MIT license. See License.md for more details.
